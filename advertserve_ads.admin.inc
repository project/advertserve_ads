<?php
/**
 * @file
 * Admin functionality for controlling the AdvertServe Ads module.
 */

/**
 * Primary FormAPI callback for building the form.
 */
function advertserve_ads_settings_form($form = array(), $form_state) {
  $form['account_id'] = array(
    '#title' => t('Account ID'),
    '#description' => t('This is used as the subdomain to load adds, e.g. if the ads are loaded from http://example.advertserve.com/ then the account ID is "example".'),
    '#type' => 'textfield',
    '#default_value' => variable_get('advertserve_ads_account_id'),
    '#field_prefix' => 'http://',
    '#field_suffix' => '.advertserve.com/',
    '#required' => TRUE,
  );

  if (empty($form['account_id']['#default_value'])) {
    drupal_set_message(t('The account ID must be set before any zones can be added.'), 'warning', FALSE);
  }

  else {
    $form['zones'] = array(
      '#type' => 'fieldset',
      '#title' => t('Ad zones'),
      '#collapsible' => FALSE,
      '#collapsed' => FALSE,
      '#description' => t('Add a new zone by filling in the blank row(s). Remove an ad zone by clearing out its zone ID value. Ad zone IDs must be numerical.'),
    );
    $form['zones']['existing'] = array(
      // Theme this part of the form as a table.
      '#theme' => 'advertserve_ads_settings_form_table',
      // Pass header information to the theme function.
      '#header' => array(
        t('Ad zone ID'),
        t('Label for this ad'),
      ),
      '#tree' => TRUE,
      'rows' => array(
        '#tree' => TRUE,
      ),
    );

    $zones = variable_get('advertserve_ads_zones', array());
    ksort($zones);

    // Display each ad zone.
    if (!empty($zones)) {
      // Add fields for each zone.
      foreach ($zones as $zone_id => $label) {
        $form['zones']['existing'][$zone_id] = array(
          'id' => array(
            '#type' => 'textfield',
            '#default_value' => $zone_id,
          ),
          'label' => array(
            '#type' => 'textfield',
            '#default_value' => $label,
          ),
        );
        $form['zones']['existing']['rows'][] = $form['zones']['existing'][$zone_id];
      }
    }

    // Add fields for adding new items.
    $form['zones']['existing']['new1'] = array(
      'id' => array(
        '#type' => 'textfield',
        '#default_value' => '',
      ),
      'label' => array(
        '#type' => 'textfield',
        '#default_value' => '',
      ),
    );
    $form['zones']['existing']['rows'][] = $form['zones']['existing']['new1'];
    $form['zones']['existing']['new2'] = array(
      'id' => array(
        '#type' => 'textfield',
        '#default_value' => '',
      ),
      'label' => array(
        '#type' => 'textfield',
        '#default_value' => '',
      ),
    );
    $form['zones']['existing']['rows'][] = $form['zones']['existing']['new2'];

    $form['advanced'] = array(
      '#type' => 'fieldset',
      '#title' => t('Advanced options'),
      '#collapsed' => TRUE,
      '#collapsible' => TRUE,
    );
    $form['advanced']['blocks'] = array(
      '#title' => t('Enable ad blocks'),
      '#type' => 'checkbox',
      '#default_value' => variable_get('advertserve_ads_blocks', TRUE),
      '#description' => t('Uncheck to disable ad display using the core <a href="!blocks">Block</a> system. Should only be disabled when using <a href="https://drupal.org/project/panels">Panels</a>, <a href="https://drupal.org/project/panelizer">Panelizer</a> or <a href="https://drupal.org/project/panels_everywhere">Panels Everywhere</a> modules to position the ads.', array('!blocks' => url('admin/structure/block'))),
    );
  }

  // Submit button.
  $form['actions'] = array();
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * FormAPI validation callback for advertserve_ads_settings_form.
 */
function advertserve_ads_settings_form_validate($form, &$form_state) {
  // Verify the ad zones.
  if (!empty($form_state['values']['existing']['rows'])) {
    foreach ($form_state['values']['existing']['rows'] as $key => $zone) {
      if (!empty($zone['id']) && !is_numeric($zone['id'])) {
        form_set_error($key . '][id', t('Ad zone IDs must be numerical, i.e. simple numbers.'));
      }
    }
  }
}

/**
 * FormAPI submission callback for advertserve_ads_settings_form.
 */
function advertserve_ads_settings_form_submit(&$form, &$form_state) {
  // Save the account ID.
  variable_set('advertserve_ads_account_id', $form_state['values']['account_id']);

  // Update the 'blocks' option.
  variable_set('advertserve_ads_blocks', !empty($form_state['values']['blocks']));

  $zones = array();

  // Save the ad zone data.
  if (!empty($form_state['values']['existing']['rows'])) {
    foreach ($form_state['values']['existing']['rows'] as $key => $zone) {
      $zone_id = $zone['id'];
      $zone_label = $zone['label'];

      // Only save zones that have an ID, i.e. allow records to be removed by
      // blanking out the ad zone.
      if (!empty($zone_id)) {
        // Default label.
        if (empty($zone_label)) {
          $zone_label = $zone_id;
        }
        $zones[$zone_id] = $zone_label;
      }
    }

    // Sort the ad zones so they're listed by zone ID.
    if (!empty($zones)) {
      ksort($zones);
    }
  }

  // Save whatever zones exist.
  variable_set('advertserve_ads_zones', $zones);
}

/**
 * Theme callback for the form table.
 */
function theme_advertserve_ads_settings_form_table(&$variables) {
  // Get the useful values.
  $form = $variables['form'];
  $rows = $form['rows'];
  $header = $form['#header'];

  // Setup the structure to be rendered and returned.
  $content = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => array(),
  );

  // Traverse each row.  @see element_chidren().
  foreach (element_children($rows) as $row_index) {
    $row = array();
    // Traverse each column in the row.  @see element_children().
    foreach (element_children($rows[$row_index]) as $col_index) {
      // Render the column form element.
      $row[] = drupal_render($rows[$row_index][$col_index]);
    }
    // Add the row to the table.
    $content['#rows'][] = $row;
  }

  // Render the table and return.
  return drupal_render($content);
}
