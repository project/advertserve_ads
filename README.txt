AdvertServe Ads
---------------
Display advertisements from the AdvertServe [1] ad provider.


Configuration & usage
--------------------------------------------------------------------------------
One permission ('Administer AdvertServe Ads') is used to control access to the
settings page.

The main settings page is found at:
  Configuration -> Web services -> AdvertServe Ads

Loading the setings page for the first time will show one option: Account ID

This value is taken from the URL used to load ads from AdvertServe. If the URL
is http://example.advertserve.com/ then the account ID is "example". This can
also be taken from the URL to log into the AdvertServe account, e.g. of the
control panel login is http://example.advertserve.com/servlet/control then the
account ID is "example".

Fill in the account ID and click "Save" to continue through to adding ad zones.

Once the account ID is saved, a new section to the form will be made available
for inserting ads into the site. All that is necessary to insert an ad is to
fill in the "zone ID", the label may be assigned if necessary. If there is not
enough space to fill in all of the necessary zone IDs, clicking Save will add
two more fields to the page.

Placement of the ads can be done via either blocks or Panels panes. In both
cases a separate block or pane will be made available for each ad in the site,
just assign their placement as usual with the repective display system.


Advanced options
--------------------------------------------------------------------------------
By default the module makes each ad available as a separate block, for use with
the normal block display system, Context and Display Suite, etc, and as separate
CTools content_type plugins for use with Panels, Panelizer, etc. When using the
Panels system it can save some confusion by unchecking the "Enable ad blocks"
option on the settings page; this will remove the normal blocks, leaving only
the Panels panes.


Credits / Contact
--------------------------------------------------------------------------------
Written and maintained by Damien McKenna [1]. Development is sponsored by
Mediacurrent [2].

The best way to contact the authors is to submit an issue, be it a support
request, a feature request or a bug report, in the project issue queue:
  https://drupal.org/project/issues/advertserve_ads


References
--------------------------------------------------------------------------------
1: http://www.advertserve.com/
2: https://drupal.org/u/damienmckenna
3: http://www.mediacurrent.com/
