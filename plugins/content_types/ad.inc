<?php
/**
 * @file
 * CTools integration for AdvertServe.
 */

/**
 * Implementation of hook_ctools_content_types().
 */
function advertserve_ads_ad_ctools_content_types() {
  return array(
    'title' => t('AdvertServe Ads'),
    'all contexts' => TRUE,
  );
}

/**
 * Return all content types available.
 */
function advertserve_ads_ad_content_type_content_types($plugin) {
  $types = array();

  $zones = variable_get('advertserve_ads_zones', array());

  foreach ($zones as $zone_id => $label) {
    $types[$zone_id] = array(
      'title' => advertserve_ads_zone_label($zone_id),
      'category' => 'Ads',    
    );
  }

  return $types;
}

/**
 * Implementation of hook_ctools_content_type_render().
 */
function advertserve_ads_ad_content_type_render($subtype, $conf, $panel_args, $contexts) {
  // Build the arguments to be passed to the template.
  $vars = array(
    'account_id' => variable_get('advertserve_ads_account_id'),
    'zone' => $subtype,
    'pid' => 0,
  );

  // Build a block-like object to display this pane.
  $block = new StdClass;
  $block->content = theme('advertserve_ad', $vars);
  $block->title = '';
  $block->delta = $subtype;
  $block->css_class = 'adverserve-ad advertserve-' . $subtype;
  return $block;
}

/**
 * Implementation of hook_ctools_content_type_admin_title().
 */
function advertserve_ads_ad_content_type_admin_title($subtype, $conf, $contexts) {
  return t('AdvertServe Ad');
}

/**
 * Implementation of hook_ctools_content_type_admin_info().
 */
function advertserve_ads_ad_content_type_admin_info($subtype, $conf, $contexts) {
  $zones = variable_get('advertserve_ads_zones', array());

  $block = new StdClass;
  $block->title = t('Zone') . ': ' . $zones[$subtype];
  $block->content = t('Display ad zone #!zone_id.', array('!zone_id' => $subtype)) . '<br />'
    . l(t('Manage AdvertServe Ads settings'), 'admin/config/services/advertserve-ads') . '.';
  return $block;
}
